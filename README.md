# Инженерное проектирование 
## Филиппов Вячеслав Евгеньевич 181-322

### Для входа используйте следующие данные:

## Админ 1.
### Логин: Viacheslav
### Пароль: Mospolytech2021

## Админ 2.
### Логин: MDanshina
### Пароль: Mospolytech2021

## Как запустить

Все команды прописываем в терминале git bash или в обычном, если это Linux или Mac, в папке проекта.

1. Установить python от 3й версии, не меньше, venv
2. Проверяем версию с помощью python --version, если она третья, то пишем в начале `python`, а не `python3`, аналогично с `pip`
3. python3 -m venv venv
4. source venv/scripts/activate
5. pip3 install -r requirements.txt
6. python3 manage.py migrate
7. python3 manage.py runserver

Далее переходим по локальному адресу - http://127.0.0.1:8000/