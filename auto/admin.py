from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from django.urls import reverse
from django.utils.http import urlencode
from django.utils.html import format_html
from .models import User, Mark, Model, Review, News, Car, Analytics, Category, Conversation, Message

def make_archived(modeladmin, request, queryset):
    queryset.update(is_archived=True)
def make_unarchived(modeladmin, request, queryset):
    queryset.update(is_archived=False)

def make_deleted(modeladmin, request, queryset):
    queryset.update(is_deleted=True)
def make_undeleted(modeladmin, request, queryset):
    queryset.update(is_deleted=False)

class UserResource(resources.ModelResource):
    class Meta:
        model = User

class MarkResource(resources.ModelResource):
    class Meta:
        model = Mark

class ModelResource(resources.ModelResource):
    class Meta:
        model = Model

class ReviewResource(resources.ModelResource):
    class Meta:
        model = Review

class NewsResource(resources.ModelResource):
    class Meta:
        model = News

class CarResource(resources.ModelResource):
    class Meta:
        model = Car

class AnalyticsResource(resources.ModelResource):
    class Meta:
        model = Analytics

class CategoryResource(resources.ModelResource):
    class Meta:
        model = Category

class ConversationResource(resources.ModelResource):
    class Meta:
        model = Conversation

class MessageResource(resources.ModelResource):
    class Meta:
        model = Message

@admin.register(User)
class UserAdmin(ImportExportModelAdmin):
    resource_class = UserResource
    list_display = ['name', 'phone', 'created_at']

@admin.register(Mark)
class MarkAdmin(ImportExportModelAdmin):
    resource_class = MarkResource
    search_fields=["title"]
    list_display = ['title', 'slug']

@admin.register(Model)
class ModelAdmin(ImportExportModelAdmin):
    resource_class = ModelResource
    search_fields=["title"]
    list_display = ['mark','issue_year', 'title', 'slug']

@admin.register(Review)
class ReviewAdmin(ImportExportModelAdmin):
    resource_class = ReviewResource
    list_display = ['author','model','title','rating','created_at']

@admin.register(News)
class NewsAdmin(ImportExportModelAdmin):
    resource_class = NewsResource
    list_display = ['author','model','mark','title','created_at']

@admin.register(Car)
class CarAdmin(ImportExportModelAdmin):
    resource_class = CarResource
    actions=[make_archived, make_unarchived]
    list_filter=["is_used", "is_available"]
    search_fields=["model"]
    list_display = ['seller', 'model', 'steering_wheel', 'power', 'engine_volume', 'body_type','rating','is_archived','is_used','is_available','updated','price','prev_price']

@admin.register(Analytics)
class AnalyticsAdmin(ImportExportModelAdmin):
    resource_class = AnalyticsResource
    list_display = ['car', 'views', 'showed_contacts']

@admin.register(Category)
class CategoryAdmin(ImportExportModelAdmin):
    resource_class = CategoryResource
    list_display = ['title']

@admin.register(Conversation)
class ConversationAdmin(ImportExportModelAdmin):
    resource_class = ConversationResource
    list_display = ['member1', 'member2', 'car', 'is_archived']
    actions=[make_archived, make_unarchived]

@admin.register(Message)
class MessageAdmin(ImportExportModelAdmin):
    resource_class = MessageResource
    list_display = ['chat', 'sender', 'is_deleted', 'sent', 'body']
    actions=[make_deleted, make_undeleted]