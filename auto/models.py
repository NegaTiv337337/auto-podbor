from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils.timezone import now
from django.db.models.functions import Lower

class User(models.Model):
    name = models.CharField(max_length=70)
    phone = models.CharField(max_length=20)
    created_at = models.DateTimeField(auto_created=True)
    def __str__(self):
        return f"{self.name}"

class Mark(models.Model):
    title = models.CharField(max_length=20)
    slug = models.CharField(max_length=10)
    def __str__(self):
        return f"{self.title}"

class Model(models.Model):
    mark = models.ForeignKey(Mark, on_delete=models.CASCADE)
    issue_year = models.PositiveSmallIntegerField()
    title = models.CharField(max_length=70)
    slug = models.SlugField()
    def __str__(self):
        return f"{self.mark}, {self.title}"

class Review(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    model = models.ForeignKey(Model, on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    body = models.TextField(blank=True)
    rating = models.FloatField(default=3.0)
    created_at = models.DateTimeField(auto_created=True)

class News(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    model = models.ForeignKey(Model, on_delete=models.CASCADE, blank=True, null=True)
    mark = models.ForeignKey(Mark, on_delete=models.CASCADE, blank=True, null=True)
    title = models.CharField(max_length=50)
    body = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_created=True, blank=True, null=True)

class Category(models.Model):
    title = models.CharField(max_length=100)
    def __str__(self):
        return f"{self.title}"

class Car(models.Model):
    STEERING_WHEEL_CHOICES = [
        ('RIGHT', 'right'),
        ('LEFT', 'left')
    ]
    BODY_TYPE_CHOICES = [
        ('hatchback', 'hatchback'),
        ('sedan', 'sedan'),
        ('coupe', 'coupe'),
        ('bus', 'bus')
    ]
    seller = models.ForeignKey(User, on_delete=models.CASCADE)
    model = models.ForeignKey(Model, on_delete=models.CASCADE)
    steering_wheel = models.CharField(
        max_length=20,
        choices=STEERING_WHEEL_CHOICES,
        default='LEFT',
    )
    category = models.ForeignKey(Category, on_delete=models.CASCADE, blank=True, null=True)
    power = models.IntegerField(blank=True)
    engine_volume = models.FloatField(blank=True)
    body_type = models.CharField(max_length=30, choices=BODY_TYPE_CHOICES, blank=True)
    summary = models.CharField(max_length=255)
    rating = models.FloatField(default=3.0)
    is_archived = models.BooleanField(default=False)
    is_used = models.BooleanField(default=False)
    is_available = models.BooleanField(default=False)
    description = models.TextField(blank=True)
    photo_url = models.CharField(blank=True, max_length=1024)
    updated = models.DateTimeField(auto_now_add=True)
    price = models.FloatField(default=0)
    prev_price = models.FloatField(blank=True, null=True)
    def __str__(self):
        return f"{self.model}, {self.engine_volume} от {self.seller}"

class Analytics(models.Model):
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    views = models.IntegerField(default=0)
    showed_contacts = models.IntegerField(default=0)
    
class Conversation(models.Model):
    member1 = models.ForeignKey(User, on_delete=models.CASCADE, related_name='member1')
    member2 = models.ForeignKey(User, on_delete=models.CASCADE, related_name='member2')
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    is_archived = models.BooleanField(default=False)
    def __str__(self):
        return f"{self.member1} – {self.member2}: {self.car}"

class Message(models.Model):
    MEMBER1 = 'member1'
    MEMBER2 = 'member2'
    SENDER_CHOICES = [
        (MEMBER1, MEMBER1),
        (MEMBER2, MEMBER2)
    ]
    chat = models.ForeignKey(Conversation, on_delete=models.CASCADE)
    sender = models.CharField(max_length=20, choices=SENDER_CHOICES, default=MEMBER1)
    is_deleted = models.BooleanField(default=False)
    sent = models.DateTimeField(auto_created=True, default=now)
    body = models.TextField()