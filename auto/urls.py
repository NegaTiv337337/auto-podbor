from django.contrib import admin
from django.urls import path, include
from django.urls.conf import re_path
from django_filters.rest_framework import DjangoFilterBackend
from django_filters import rest_framework as djangofilters
from rest_framework import routers, serializers, viewsets, filters, generics

from .models import Car, User, Model, Review, News, Category, Conversation, Message, Analytics, Mark

class MarkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mark
        fields = '__all__'

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

class ModelSerializer(serializers.ModelSerializer):
    mark = MarkSerializer()
    class Meta:
        model = Model
        fields = '__all__'

class CarSerializer(serializers.ModelSerializer):
    seller = UserSerializer()
    model = ModelSerializer()
    category = CategorySerializer()
    class Meta:
        model = Car
        fields = '__all__'

class ReviewSerializer(serializers.ModelSerializer):
    author = UserSerializer()
    model = ModelSerializer()
    class Meta:
        model = Review
        fields = '__all__'

class NewsSerializer(serializers.ModelSerializer):
    author = UserSerializer()
    class Meta:
        model = News
        fields = '__all__'

class ConversationSerializer(serializers.ModelSerializer):
    member1 = UserSerializer()
    member2 = UserSerializer()
    car = CarSerializer()
    class Meta:
        model = Conversation
        fields = '__all__'

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'

class AnalyticsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Analytics
        fields = '__all__'

class CarFilter(djangofilters.FilterSet):
    min_price = djangofilters.NumberFilter(field_name="price", lookup_expr='gte')
    max_price = djangofilters.NumberFilter(field_name="price", lookup_expr='lte')

    class Meta:
        model = Car
        fields = ['model__mark__slug', 'model__issue_year', 'is_used', 'is_available']

class CarViewSet(viewsets.ModelViewSet):
    queryset = Car.objects.all()
    serializer_class = CarSerializer
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    filterset_class = CarFilter
    search_fields = ['model__title', 'model__mark__title']

class ReviewViewSet(viewsets.ModelViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer

class MarkViewSet(viewsets.ModelViewSet):
    queryset = Mark.objects.all()
    serializer_class = MarkSerializer

class ModelsViewSet(generics.ListAPIView):
    serializer_class = ModelSerializer
    authentication_classes = []
    def get_queryset(self):
        mark = self.kwargs['mark']
        return Model.objects.filter(mark__slug=mark)
class NewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all()
    serializer_class = NewsSerializer

class AnalyticsViewSet(viewsets.ModelViewSet):
    queryset = Analytics.objects.all()
    serializer_class = AnalyticsSerializer

class ConversationsViewSet(viewsets.ModelViewSet):
    queryset = Conversation.objects.all()
    serializer_class = ConversationSerializer

router = routers.DefaultRouter()

router.register(r'marks', MarkViewSet)
router.register(r'cars', CarViewSet)
router.register(r'reviews', ReviewViewSet)
router.register(r'analytics', AnalyticsViewSet)
router.register(r'news', NewsViewSet)

modelsPath = re_path('^api/models/(?P<mark>.+)/$', ModelsViewSet.as_view())

urlpatterns = [
    modelsPath,
    path('api/', include(router.urls)),
    path('', admin.site.urls),
]