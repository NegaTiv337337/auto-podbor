import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { QueryClientProvider } from "react-query";
import { Header } from "./components/Header/Header";
import { Footer } from "./components/Footer/Footer";
import { HomePage } from "./pages/Home/HomePage";
import { history, queryClient } from "./common";
import { ModelsPage } from "./pages/Models/ModelsPage";
import { ReviewsPage } from "./pages/Reviews/ReviewsPage";
import { CatalogPage } from "./pages/Catalog/CatalogPage";
import { NewsPage } from "./pages/News/NewsPage";

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Router>
        <div>
          <Header />
          <Switch>
            <Route path="/models/:mark/:title">
              <ModelsPage />
            </Route>
            <Route path="/reviews/:model?">
              <ReviewsPage />
            </Route>
            <Route path="/news">
              <NewsPage />
            </Route>
            <Route path="/catalog">
              <CatalogPage />
            </Route>
            <Route path="/">
              <HomePage />
            </Route>
          </Switch>
          <Footer />
        </div>
      </Router>
    </QueryClientProvider>
  );
}

export default App;
