import axios from "axios";

export class API {
  static http = axios.create({
    baseURL: "http://ip-6-sem.std-689.ist.mospolytech.ru/api",
  });

  static getMarks() {
    return API.http.get("/marks/").then((data) => data.data);
  }

  static getCars({
    search,
    mark,
    issueYear,
    isUsed,
    isAvailable,
    minPrice,
    maxPrice,
  }) {
    return API.http
      .get(
        `/cars/?search=${search}&model__mark__slug=${mark}&model__issue_year=${issueYear}&is_used=${isUsed}&is_available=${isAvailable}&min_price=${minPrice}&max_price=${maxPrice}`
      )
      .then((data) => data.data);
  }

  static getReviews() {
    return API.http.get("/reviews/").then((data) => data.data);
  }

  static getNews() {
    return API.http.get("/news/").then((data) => data.data);
  }

  static getModelsByMark(mark) {
    return API.http.get(`/models/${mark}/`).then((data) => data.data);
  }
}
