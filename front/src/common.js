import { createBrowserHistory } from "history";
import { QueryClient } from "react-query";

export const history = createBrowserHistory();

export const queryClient = new QueryClient();

export function debounce(func, wait, immediate) {
  let timeout;

  return function executedFunction() {
    const context = this;
    const args = arguments;

    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    const callNow = immediate && !timeout;

    clearTimeout(timeout);

    timeout = setTimeout(later, wait);

    if (callNow) func.apply(context, args);
  };
}

export const normalizeFilters = (filters) =>
  Object.fromEntries(
    Object.entries(filters).map(([key, value]) => [key, value ? value : ""])
  );
