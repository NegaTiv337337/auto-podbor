import React from 'react';

export const Footer = () => {
  return (
    <footer
      className="flex pb-5 px-3 m-auto pt-5 
            text-gray-800 text-sm 
            flex-col md:flex-row bg-gray-200"
    >
      <div className="text-center w-full">
        © Филлипов Вячеслав 181-322. Все права защищены.
      </div>
    </footer>
  );
};
