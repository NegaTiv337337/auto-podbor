import React from "react";
import { Link } from "react-router-dom";

import { Search } from "../Search/Search";

export const Header = () => {
  return (
    <nav className="flex items-center bg-red-700 p-3 flex-wrap">
      <Link to="/" className="p-2 mr-4 inline-flex items-center">
        <span className="text-xl text-white font-bold uppercase tracking-wide">
          ВсеМашины.ру
        </span>
      </Link>
      <button
        className="text-white inline-flex p-3 hover:bg-gray-900 rounded lg:hidden ml-auto hover:text-white outline-none nav-toggler"
        data-target="#navigation"
      >
        <i className="material-icons">menu</i>
      </button>
      <div
        className="hidden top-navbar w-full lg:inline-flex lg:flex-grow lg:w-auto"
        id="navigation"
      >
        <div className="lg:inline-flex lg:flex-row lg:ml-auto lg:w-auto w-full lg:items-center items-start flex flex-col lg:h-auto">
          <Search />
          <Link
            to="/catalog"
            className="lg:inline-flex lg:w-auto w-full px-3 py-2 rounded text-gray-200 items-center justify-center hover:bg-gray-900 hover:text-white"
          >
            <span>Объявления</span>
          </Link>
          <Link
            to="/reviews"
            className="lg:inline-flex lg:w-auto w-full px-3 py-2 rounded text-gray-200 items-center justify-center hover:bg-gray-900 hover:text-white"
          >
            <span>Обзоры</span>
          </Link>
          <Link
            to="/news"
            className="lg:inline-flex lg:w-auto w-full px-3 py-2 rounded text-gray-200 items-center justify-center hover:bg-gray-900 hover:text-white"
          >
            <span>Новости</span>
          </Link>
        </div>
      </div>
    </nav>
  );
};
