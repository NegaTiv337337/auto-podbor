import React from "react";
import { Header } from "./components/Header/Header";
import { Footer } from "./components/Footer/Footer";

export const Layout = () => {
  return (
    <div>
      <Header />
      <main></main>
      <Footer />
    </div>
  );
};
