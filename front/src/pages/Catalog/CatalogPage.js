import React, { useState } from "react";
import { useParams, Link, useLocation } from "react-router-dom";
import { useQuery } from "react-query";
import { Range } from "react-range";
import Loader from "react-loader-spinner";
import { RadioGroup, RadioButton } from "react-radio-buttons";
import { API } from "../../api";
import { normalizeFilters } from "../../common";

const handleBodyType = (type) => {
  switch (type) {
    case "coupe":
      return "Купе";
    case "hatchback":
      return "Хэчбэк";
    case "bus":
      return "Автобус";
    default:
      return "Обычный";
  }
};

export const CatalogPage = () => {
  const location = useLocation();
  const params = new URLSearchParams(location.search);
  const search = params.get("search");
  const mark = params.get("mark");
  const [filters, setFilters] = useState({
    search,
    mark,
    issueYear: null,
    isUsed: true,
    isAvailable: null,
    minPrice: 50000,
    maxPrice: 10000000,
  });
  const { isLoading, data } = useQuery(["cars", filters], () =>
    API.getCars(normalizeFilters(filters))
  );
  const onFilterChange = (field) => (value) => {
    if (["isUsed"].includes(field)) {
      setFilters((filters) => ({ ...filters, [field]: value }));
      return;
    }
    if (field === "price") {
      setFilters((filters) => ({
        ...filters,
        minPrice: value[0],
        maxPrice: value[1],
      }));
      return;
    }
    setFilters((filters) => ({ ...filters, [field]: value.target.value }));
  };
  console.log(data, filters, "catalog");
  return (
    <article
      className="px-4 py-8 mx-auto max-w-7xl"
      itemid="#"
      itemscope
      itemtype="http://schema.org/BlogPosting"
      style={{ minHeight: "calc(100vh - 128px)" }}
    >
      <div className="container mx-auto mb-8 text-left">
        <h1
          className="mb-3 text-3xl font-bold leading-tight text-gray-900 md:text-4xl"
          itemprop="headline"
        >
          Автомобили {search || mark} на продажу
        </h1>
      </div>
      <div className="container grid gap-16 catalog">
        <main>
          <div className="sm:max-w-xl md:max-w-full lg:max-w-screen-xl">
            <div className="grid gap-8 row-gap-5">
              {isLoading || !data ? (
                <Loader
                  type="TailSpin"
                  color="#00BFFF"
                  className="justify-self-center"
                  height={100}
                  width={100}
                />
              ) : data.length > 0 ? (
                data.map(
                  ({
                    body_type,
                    category,
                    engine_volume,
                    is_archived,
                    is_available,
                    is_used,
                    model,
                    photo_url,
                    power,
                    price,
                    prev_price,
                    rating,
                    seller,
                    steering_wheel,
                    summary,
                    updated,
                  }) => (
                    <div className="relative p-px overflow-hidden transition duration-300 transform border rounded shadow-sm hover:scale-105 group hover:shadow-xl">
                      <div className="absolute bottom-0 left-0 w-full h-1 duration-300 origin-left transform scale-x-0 bg-deep-purple-accent-400 group-hover:scale-x-100" />
                      <div className="absolute bottom-0 left-0 w-1 h-full duration-300 origin-bottom transform scale-y-0 bg-deep-purple-accent-400 group-hover:scale-y-100" />
                      <div className="absolute top-0 left-0 w-full h-1 duration-300 origin-right transform scale-x-0 bg-deep-purple-accent-400 group-hover:scale-x-100" />
                      <div className="absolute bottom-0 right-0 w-1 h-full duration-300 origin-top transform scale-y-0 bg-deep-purple-accent-400 group-hover:scale-y-100" />
                      <div className="relative flex flex-col h-full p-5 bg-white rounded-sm lg:items-center lg:flex-row">
                        <div
                          style={{ width: 270 }}
                          className="mb-6 mr-6 lg:mb-0"
                        >
                          <img
                            src={photo_url}
                            style={{ minHeight: 100 }}
                            className="flex items-center justify-center bg-indigo-50 object-fill"
                          />
                        </div>
                        <div className="flex flex-col justify-between flex-grow">
                          <div>
                            <h6 className="font-semibold leading-5">
                              {model.title} ({model.issue_year} г.){" "}
                              {is_used && (
                                <span class="ml-2 text-xs font-medium bg-red-100 py-1 px-2 rounded text-red-500 align-middle">
                                  Вторичка
                                </span>
                              )}{" "}
                              {!is_used && (
                                <span class="ml-2 text-xs font-medium bg-green-100 py-1 px-2 rounded text-green-500 align-middle">
                                  Новая
                                </span>
                              )}
                            </h6>
                            <div class="flex items-center mt-2 mb-4">
                              <svg
                                class={`-ml-0.5 mx-1 w-4 h-4 fill-current text-${
                                  rating > 0 ? "yellow" : "gray"
                                }-500`}
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 20 20"
                              >
                                <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                              </svg>
                              <svg
                                class={`mx-1 w-4 h-4 fill-current text-${
                                  rating > 1 ? "yellow" : "gray"
                                }-500`}
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 20 20"
                              >
                                <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                              </svg>
                              <svg
                                class={`mx-1 w-4 h-4 fill-current text-${
                                  rating > 2 ? "yellow" : "gray"
                                }-500`}
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 20 20"
                              >
                                <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                              </svg>
                              <svg
                                class={`mx-1 w-4 h-4 fill-current text-${
                                  rating > 3 ? "yellow" : "gray"
                                }-500`}
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 20 20"
                              >
                                <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                              </svg>
                              <svg
                                class={`mx-1 w-4 h-4 fill-current text-${
                                  rating > 4 ? "yellow" : "gray"
                                }-500`}
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 20 20"
                              >
                                <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                              </svg>
                            </div>
                            <p className="text-xs text-gray-500">
                              {engine_volume} л / {power} л.с. /{" "}
                              {handleBodyType(body_type)} /{" "}
                              {steering_wheel === "LEFT"
                                ? "Левый руль"
                                : "Правый руль"}
                            </p>
                            <p className="mt-2 mb-2 text-sm text-gray-900">
                              {summary}
                            </p>
                          </div>
                          <p className="inline-flex items-center text-md font-semibold transition-colors duration-200 text-deep-purple-accent-400 hover:text-deep-purple-800">
                            {price.toLocaleString()} руб.
                            {prev_price && (
                              <span className="text-gray-400 text-sm line-through ml-2">
                                {prev_price.toLocaleString()} руб.
                              </span>
                            )}
                          </p>
                          <Link
                            to={`/reviews/${model.title}`}
                            style={{ maxWidth: 160 }}
                            className="mt-4 text-sm bg-red-500 hover:bg-red-900 text-white font-bold py-2 px-4 rounded w-auto"
                          >
                            Почитать отзывы
                          </Link>
                        </div>
                      </div>
                    </div>
                  )
                )
              ) : (
                <h2>Машин не нашлось :(</h2>
              )}
            </div>
          </div>
        </main>
        <aside>
          <div className="flex flex-col px-4">
            <p className="text-gray-800 dark:text-gray-100 text-sm font-bold leading-tight tracking-normal mb-12">
              Цена (руб.)
            </p>
            <Range
              draggableTrack
              values={[filters.minPrice, filters.maxPrice]}
              step={10000}
              min={50000}
              max={10000000}
              onChange={onFilterChange("price")}
              renderTrack={({ props, children }) => (
                <div
                  {...props}
                  style={{
                    ...props.style,
                    height: "6px",
                    width: "100%",
                    backgroundColor: "#ccc",
                  }}
                >
                  {children}
                </div>
              )}
              renderThumb={({ index, props, isDragged }) => (
                <div
                  {...props}
                  style={{
                    ...props.style,
                    height: "20px",
                    width: "20px",
                    borderRadius: "4px",
                    backgroundColor: "#FFF",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    boxShadow: "0px 2px 6px #AAA",
                  }}
                >
                  <div
                    style={{
                      position: "absolute",
                      top: "-32px",
                      color: "#fff",
                      fontWeight: "bold",
                      fontSize: "14px",
                      padding: "2px 4px",
                      borderRadius: "4px",
                      backgroundColor: "#ba1c1d",
                    }}
                  >
                    {(index > 0
                      ? filters.maxPrice
                      : filters.minPrice
                    ).toLocaleString()}
                  </div>
                  <div
                    style={{
                      height: "10px",
                      width: "5px",
                      backgroundColor: isDragged ? "#ba1c1d" : "#CCC",
                    }}
                  />
                </div>
              )}
            />
            <div className="flex flex-col md:mr-16 mb-4 mt-6">
              <label
                htmlFor="modelOrMark"
                className="text-gray-800 dark:text-gray-100 text-sm font-bold leading-tight tracking-normal mb-2"
              >
                Модель или марка
              </label>
              <input
                onChange={onFilterChange("search")}
                value={filters.search}
                id="modelOrMark"
                className="text-gray-600 dark:text-gray-400 focus:outline-none focus:border focus:border-red-700 dark:focus:border-indigo-700 dark:border-gray-700 dark:bg-gray-800 bg-white font-normal w-64 h-10 flex items-center pl-3 text-sm border-gray-300 rounded border shadow"
                placeholder="Lada"
              />
            </div>
            <div className="flex flex-col md:mr-16 mb-4">
              <label
                htmlFor="issueYear"
                className="text-gray-800 dark:text-gray-100 text-sm font-bold leading-tight tracking-normal mb-2"
              >
                Год выпуска
              </label>
              <input
                onChange={onFilterChange("issueYear")}
                value={filters.issueYear}
                id="issueYear"
                className="text-gray-600 dark:text-gray-400 focus:outline-none focus:border focus:border-red-700 dark:focus:border-indigo-700 dark:border-gray-700 dark:bg-gray-800 bg-white font-normal w-64 h-10 flex items-center pl-3 text-sm border-gray-300 rounded border shadow"
                placeholder="2020"
              />
            </div>
            <p className="text-gray-800 dark:text-gray-100 text-sm font-bold leading-tight tracking-normal mb-4">
              Новизна
            </p>
            <RadioGroup
              value={filters.isUsed}
              onChange={onFilterChange("isUsed")}
            >
              <RadioButton pointColor="#ba1c1d" value="true">
                Вторичка
              </RadioButton>
              <RadioButton pointColor="#ba1c1d" value="false">
                Новая
              </RadioButton>
            </RadioGroup>
          </div>
        </aside>
      </div>
    </article>
  );
};
