import React from "react";
import { Link } from "react-router-dom";
import { useQuery } from "react-query";
// import { useQuery } from "../../use-query";
import Loader from "react-loader-spinner";
import { API } from "../../api";

const splitToChunks = (array, parts) => {
  if (!array) return [[], [], [], []];
  let result = [];
  for (let i = parts; i > 0; i--) {
    result.push(array.splice(0, Math.ceil(array.length / i)));
  }
  return result;
};

export const HomePage = () => {
  const { isLoading, data, error } = useQuery("marks", API.getMarks);
  const [firstCol, secondCol, thirdCol, fourthCol] = splitToChunks(
    data ? [...data] : [],
    4
  );
  return (
    <article
      className="px-4 py-24 mx-auto max-w-7xl"
      itemid="#"
      itemscope
      itemtype="http://schema.org/BlogPosting"
      style={{ minHeight: "calc(100vh - 128px)" }}
    >
      <div className="mx-auto mb-8 text-left w-3/4">
        <h1
          className="mb-3 text-3xl font-bold leading-tight text-gray-900 md:text-4xl"
          itemprop="headline"
        >
          Доска объявлений автомобилей
        </h1>
      </div>
      <div className="mx-auto prose w-3/4">
        <p>Выберите марку желаемого автомобиля</p>
      </div>
      <div className="container w-3/4 mx-auto mt-8 bg-grey-lighter">
        {isLoading ? (
          <Loader
            type="TailSpin"
            color="#00BFFF"
            className="justify-self-center"
            height={100}
            width={100}
          />
        ) : (
          <div className="sm:flex mb-4">
            <div className="sm:w-1/4 h-auto">
              <ul className="list-reset leading-normal">
                {firstCol.map(({ slug, title }) => (
                  <li
                    key={slug}
                    className="hover:text-black text-blue-500 mb-2"
                  >
                    <Link to={`/models/${slug}/${title}`}>{title}</Link>
                  </li>
                ))}
              </ul>
            </div>
            <div className="sm:w-1/4 h-auto sm:mt-0 mt-8">
              <ul className="list-reset leading-normal">
                {secondCol.map(({ slug, title }) => (
                  <li
                    key={slug}
                    className="hover:text-black text-blue-500 mb-2"
                  >
                    <Link to={`/models/${slug}/${title}`}>{title}</Link>
                  </li>
                ))}
              </ul>
            </div>
            <div className="sm:w-1/4 h-auto sm:mt-0 mt-8">
              <ul className="list-reset leading-normal">
                {thirdCol.map(({ slug, title }) => (
                  <li
                    key={slug}
                    className="hover:text-black text-blue-500 mb-2"
                  >
                    <Link to={`/models/${slug}/${title}`}>{title}</Link>
                  </li>
                ))}
              </ul>
            </div>
            <div className="sm:w-1/4 sm:mt-0 mt-8 h-auto">
              <ul className="list-reset leading-normal">
                {fourthCol.map(({ slug, title }) => (
                  <li
                    key={slug}
                    className="hover:text-black text-blue-500 mb-2"
                  >
                    <Link to={`/models/${slug}/${title}`}>{title}</Link>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        )}
      </div>
      <div className="mx-auto prose w-3/4 mt-8">
        <p>
          Или сразу посмотрите объявления в{" "}
          <Link to="/catalog" className="text-blue-500">
            каталоге
          </Link>
        </p>
      </div>
    </article>
  );
};
