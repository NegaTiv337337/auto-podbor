import React from "react";
import { useQuery } from "react-query";
import Loader from "react-loader-spinner";
import { Link, useParams } from "react-router-dom";
import { API } from "../../api";

export const ModelsPage = () => {
  const { mark, title } = useParams();
  const { isLoading, data, error } = useQuery("models", () =>
    API.getModelsByMark(mark)
  );
  console.log(data, "modelspage");
  return (
    <div
      className="container w-3/4 mx-auto"
      style={{ minHeight: "calc(100vh - 158px)" }}
    >
      <div className="my-8 text-left">
        <h1
          className="mb-3 text-3xl font-bold leading-tight text-gray-900 md:text-4xl"
          itemprop="headline"
        >
          Модели {title}
        </h1>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-6 mt-8">
        {isLoading || !data ? (
          <Loader
            type="TailSpin"
            color="#00BFFF"
            className="justify-self-center"
            height={100}
            width={100}
          />
        ) : (
          data.map(({ issue_year, title, slug }) => (
            <Link
              key={slug + issue_year + title}
              class="card shadow-lg my-8 hover:bg-gray-200"
              to={`/reviews/${title}`}
            >
              <div class="card-body round-4 p-4">
                <h2 class="font-semibold leading-5 m-0">
                  {title}
                  <span class="ml-2 text-xs font-medium bg-red-100 py-1 px-2 rounded text-red-500 align-middle">
                    {issue_year}
                  </span>
                </h2>
              </div>
            </Link>
          ))
        )}
      </div>
    </div>
  );
};
