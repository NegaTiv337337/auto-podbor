import React from "react";
import { useQuery } from "react-query";
import { Link } from "react-router-dom";
import { API } from "../../api";

export const NewsPage = () => {
  const { isLoading, data, error } = useQuery("news", API.getNews);
  return (
    <div className="container w-3/4 mx-auto my-8">
      <h1
        className="mb-8 text-3xl font-bold leading-tight text-gray-900 md:text-4xl"
        itemprop="headline"
      >
        Новости мира автопрома
      </h1>
      {(data || []).map(({ author, created_at, title, body, rating }) => (
        <div
          key={created_at}
          class="max-w-4xl px-10 py-6 mx-auto bg-white rounded-lg shadow-md my-8"
        >
          <div class="flex items-center justify-between">
            <span class="font-light text-gray-600">
              {new Date(created_at).toLocaleString("ru")}
            </span>
          </div>
          <div class="mt-2">
            <p class="text-2xl font-bold text-gray-700">{title}</p>
            <p
              class="mt-2 text-gray-600"
              dangerouslySetInnerHTML={{ __html: body }}
            />
          </div>
          <div class="flex items-center justify-between mt-4">
            <div>
              <a href="#" class="flex items-center">
                <img
                  src={`https://i.pravatar.cc/100?u=${author.name}`}
                  alt="avatar"
                  class="hidden object-cover w-10 h-10 mr-4 rounded-full sm:block"
                />
                <h1 class="font-bold text-gray-700 hover:underline">
                  {author.name}
                </h1>
              </a>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};
