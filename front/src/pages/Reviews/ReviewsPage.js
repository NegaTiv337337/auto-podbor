import React from "react";
import { useQuery } from "react-query";
import { useParams, Link } from "react-router-dom";
import { API } from "../../api";

export const ReviewsPage = () => {
  const { isLoading, data, error } = useQuery("reviews", API.getReviews);
  const { model } = useParams();
  console.log(model, data);
  const filteredData = data
    ? model
      ? data.filter((el) => el.model.title === model)
      : data
    : [];
  return (
    <div
      className="container w-3/4 mx-auto my-8"
      style={{ minHeight: "calc(100vh - 188px)" }}
    >
      <h1
        className="mb-8 text-3xl font-bold leading-tight text-gray-900 md:text-4xl"
        itemprop="headline"
      >
        Обзоры {model ? `на ${model}` : "автомобилей"} от пользователей
      </h1>
      {filteredData.map(
        ({ author, model, created_at, title, body, rating }) => (
          <div
            key={created_at}
            class="max-w-4xl px-10 py-6 mx-auto bg-white rounded-lg shadow-md my-8"
          >
            <div class="flex items-center justify-between">
              <span class="font-light text-gray-600">
                {new Date(created_at).toLocaleString("ru")}
              </span>
              <a
                href="#"
                class="px-2 py-1 font-bold text-gray-100 bg-red-600 rounded hover:bg-red-500"
              >
                {rating}
              </a>
            </div>
            <div class="mt-2">
              <p class="text-2xl font-bold text-gray-700">{title}</p>
              <p className="mt-2 mb-6 text-gray-400">
                {model.mark.title} {model.title} ({model.issue_year}) |
                <span className="ml-2">
                  <Link
                    className="text-blue-500"
                    to={`/catalog?search=${model.mark.slug}`}
                  >
                    Купить такую же
                  </Link>
                </span>
              </p>
              <p
                class="mt-2 text-gray-600"
                dangerouslySetInnerHTML={{ __html: body }}
              />
            </div>
            <div class="flex items-center justify-between mt-4">
              <div>
                <a href="#" class="flex items-center">
                  <img
                    src={`https://i.pravatar.cc/100?u=${author.name}`}
                    alt="avatar"
                    class="hidden object-cover w-10 h-10 mr-4 rounded-full sm:block"
                  />
                  <h1 class="font-bold text-gray-700 hover:underline">
                    {author.name}
                  </h1>
                </a>
              </div>
            </div>
          </div>
        )
      )}
      {!isLoading && filteredData.length == 0 && (
        <>
          <h3 className="text-xl leading-tight mb-8">
            К сожалению, обзоров на такую модель не нашлось...
          </h3>
          <h6>
            Посмотрите что-нибудь{" "}
            <Link to={`/catalog`} className="text-blue-400">
              в каталоге
            </Link>
          </h6>
        </>
      )}
    </div>
  );
};
