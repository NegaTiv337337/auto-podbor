import { useState, useEffect } from 'react';

export const useQuery = (promise, args) => {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    setIsLoading(true);
    promise(args)
      .then((data) => {
        setData(data);
        setIsLoading(false);
      })
      .catch((err) => {
        setError(err);
        setIsLoading(false);
      });
  }, []);
  useEffect(() => {
    console.log('data changed', data);
  }, [data]);
  return { data, error, isLoading };
};
